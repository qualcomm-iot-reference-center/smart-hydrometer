from abc import ABCMeta,abstractmethod
import sys,os


class IHydrometerCore:
	__metaclass__ = ABCMeta

	@abstractmethod
	def Process(self,frame):
		pass

