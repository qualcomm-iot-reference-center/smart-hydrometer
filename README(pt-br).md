# Hidrômetro Inteligente utilizando OpenCV e Caffe2 na DragonBoard 410C 

 
Projeto desenvolvido no IoT Referece Center, parceria entre Qualcomm e Facens.
Este artigo apresenta uma aplicação executada na DragonBoard410c, baseada nos conceitos de IA, que faz o reconhecimento de dígitos de um Hidrômetro utilizando OpenCV e Caffe2. 

 

# Introdução 

A aplicação desenvolvida utiliza uma câmera para realizar o monitoramento de um hidrômetro. Para demonstração e prova de conceito, utilizou-se uma câmera IP para possibilitar o monitoramento de forma remota. Com isso, as imagens do hidrômetro são obtidas via rede e processadas na DragonBoard410c. 


 <div align="center">
    <figure>
        <img src="/Tutorial/fig1.png">
        <figcaption>Hidrômetro utilizado no projeto.</figcaption>
    </figure>
</div>


Cabe ressaltar que a imagem obtida é dependente do ambiente de operação. Por isso, faz-se necessário o tratamento das imagens buscando uma melhor adaptação ao ambiente em que o projeto é implementado. 

Na imagem analisada é definida uma região de interesse que contém todos os dígitos do mostrador do hidrômetro. Posteriormente, cada dígito presente na imagem é segmentado e processado por uma rede convolucional (CNN) com objetivo de classificar o número. 

 

 <div align="center">
    <figure>
        <img src="/Tutorial/fig2.png">
        <figcaption>Região de interesse para reconhecimento.</figcaption>
    </figure>
</div>



 

Para o desenvolvimento do projeto, utilizou-se a biblioteca OpenCV para fazer a segmentação dos dígitos, e o framework Caffe2 para realizar o reconhecimento dos dígitos. 

Para realizar o treinamento de rede e reconhecer os dígitos no mostrador do hidrômetro, utilizou-se o dataset MNIST. 

 

É necessário realizar o treino da rede antes de executar o script do projeto. 

Para entender como ocorre o treinamento e como executar a modelo na DragonBoard 410C, acesse o artigo MNIST: Criando uma CNN para DragonBoard 410C que contém todas as instruções para instalação, treinamento e execução da rede. 

 

## Hardware Utilizado

 

> Essa aplicação foi executada na plataforma de desenvolvimento DragonBoard410c. A imagem do sistema operacional é a linaro-buster-alip-dragonboard-410c-359.img.gz e pode ser obtida neste link: https://releases.linaro.org/96boards/dragonboard410c/linaro/debian/latest/ 

 

> Versão do kernel: 

 

> Linux qubuntu 4.15.0-36-generic #39-Ubuntu SMP Mon Sep 24 16:19:09 UTC 2018 x86_64 x86_64 x86_64 GNU/Linux 

 

> Cabe ressaltar que o sistema utiliza um uma memória externa para expandir a capacidade do rootfs e fornecer uma região de swap. A configuração desse mecanismo é a seguinte: de 4GB de swap e o restante dedicado para o rootfs. 

 

### Equipamentos 

 

Os equipamentos utilizados para implementação do projeto são: 

* DragonBoard 410C; 
* Câmera IP; 
* Hidrômetro. 

 

## Bibliotecas e Frameworks 

 

As dependências de software utilizadas no projeto são: 

* OpenCV; 

* Numpy; 

* [Caffe2](https://gitlab.com/qualcomm-iot-reference-center/caffe2-installation-and-test); 

* Imutils; 

* uuid; 

* itertools; 

* Ibmiotf; 

* Tkinter; 

* Xml.dom;

* StringIO.



 

### Instalação das bibliotecas do Python 

 

Para a instalação das bibliotecas utilizadas no projeto, execute os comandos abaixo no terminal da DragonBoard410c:

```sh
pip install numpy 

pip install imutils 

pip install uuid 

pip install more-itertools 

pip install ibmiotf 

sudo apt-get install python-imaging-tk

sudo apt-get install python-opencv
```
 

#### Instalação do framework Caffe2 

 

O framework do Caffe2 necessita de uma instalação a partir do código fonte e possui outras dependências que serão solicitadas no decorrer da instalação. O artigo sobre o Caffe2 e sua instalação na DragonBoard 410C pode ser encontrado [neste link](https://gitlab.com/qualcomm-iot-reference-center/caffe2-installation-and-test/blob/master/README(pt-br).md). Se algum dos termos tratados durante esse artigo não forem conhecidos, ou para saber mais sobre Caffe2, acesse os guias do Caffe2.ai. 

 

#### IBM Cloud 

 

A biblioteca ibmiotf.device é utilizada para o envio de dados na plataforma escolhida. Se outra plataforma for escolhida para fazer o envio de dados, deve-se verificar quais são as dependências. 

 
### Estrutura da Aplicação 

 

Abaixo segue uma breve explicação das partes principais do script que foram implementadas no projeto para entendimento da utilização da visão computacional. 

 

#### Processamento de imagem 

 

Uma das partes principais do projeto é realizar o tratamento das imagens que são capturadas pela câmera. Para isso, são realizadas aplicações de filtros para redução de ruído, conversão do espaço de cores, realce, entre outros. 

A filtragem realizada na imagem é muito importante para que posteriormente faça a detecção dos contornos dos dígitos. Para isso, foram utilizados filtros que auxiliam na detecção das bordas e as mantém nítidas. 

O objetivo desses procedimentos é destacar da melhor maneira possível os dígitos segmentados que serão utilizados para o reconhecimento. A seguir é ilustrado o resultado do processamento, resultando em uma imagem preto e branco com os dígitos em destaque. 

 
 <div align="center">
    <figure>
        <img src="/Tutorial/fig3.png">
        <figcaption>Filtros aplicados para possibilitar a segmentação dos digitos.</figcaption>
    </figure>
</div>


Para o tratamento da imagem é necessário converter em escala de cinza (1 canal), aplicar filtros suavização, realce de bordas e binarização. Posteriormente, operações morfológicas são utilizadas para limpar os ruídos da imagem e dilatar os contornos encontrados. Essas técnicas e outros tipos de filtros que podem ser aplicados são encontrados nos links abaixo, que contém toda a documentação das funções disponibilizadas na biblioteca. 

> Documentação do OpenCV para as funções utilizadas: 
> > * Color space; 
> > * Laplacian; 
> > * Bilateral; 
> > * Limiarização; 
> > * Transformação Morfológica. 

 

#### Segmentação dos dígitos 

 

Após o tratamento das imagens, é possível encontrar os contornos e mapear as posições dos dígitos para permitir o processo de reconhecimento. Tal operação é realizada considerando a posição dos contornos encontrados e as referências definidas no processo de calibração das regiões de interesse. 

Assim sendo, para cada contorno identificado, calcula-se o centro geométrico (pontos em destaque) para validar se o dígito está aparecendo por completo no mostrador. Para tal, o centro geométrico é comparado com a calibração de cada dígito. 

Posteriormente, cada dígito é convertido para o padrão de entrada da rede que realizará a classificação. 


 <div align="center">
    <figure>
        <img src="/Tutorial/fig4.png">
        <figcaption>Segmentação das regiões de interesse.</figcaption>
    </figure>
</div>

 

 

> Documentação do OpenCV para as funções utilizadas: 
> > * Find Contours; 
> > * Gaussian Filtering. 

 

#### Reconhecimento dos dígitos 

 

Como dito anteriormente, a aplicação segmenta os números da imagem original gerando uma nova imagem para cada um. Em seguida, as imagens geradas são processadas para servirem de entrada do modelo treinado. 

Um dos scripts essenciais para o reconhecimento com Caffe2 é o model.py, em que é possível encontrar toda a estrutura para carregar os modelos pré-treinados. Para carregar tais modelos, a classe Caffe2Model disponibiliza o método LoadPreTrainedModels. 

Para executar o modelo é necessário preencher a entrada da rede. Tal operação é realizada por uma especialização da classe Caffe2Model, denominada Caffe2MNIST e definida no arquivo mnist.py. 

O método Run converte a imagem (roi) para o padrão de entrada da rede e preenche o blob de entrada. Posteriormente, a rede é executada tendo como saída uma estimativa percentual para cada dígito. Assim sendo, considera-se o índice de maior valor como o dígito reconhecido. 

```python
res = model.run_caffe2(roi) * 100 


max_class = np.argmax(res) 
```
 

Assim, para cada dígito na região de classificação, obtém-se um valor percentual. O resultado será como o da figura abaixo. 


 <div align="center">
    <figure>
        <img src="/Tutorial/fig5.png">
        <figcaption>Resultado de classificação.</figcaption>
    </figure>
</div>

 

Cabe ressaltar que o programa só realiza a confirmação de um dígito com exatidão acima de 85%. Além disso, algumas amostras são armazenadas para que a tomada de decisão seja realizada com maior confiança. 

 

## Executando o Código da DragonBoard 

 
A seguir estão os passos necessários para executar o código na DragonBoard 410C. Certifique-se de que todas as bibliotecas foram instaladas e de que foi realizado o download do repositório, passos que estão no início desse tutorial e devem ser executados por primeiro.

Além disso, o programa contém uma interface gráfica. Assim sendo, é possível executá-la de duas maneiras:

1. Com a dragonboard conectada em um monitor pela porta HDMI.
2. Com uma conexão via ssh (taxa de atualização do vídeo é baixa)

Para conexão SSH, basta executar no HOST o seguinte comando:

```sh
ssh -X <USER>:<IP TARGET>
```

Em que, USER é o usuário na dragonboard, e IP é o endereço de IP da placa.


### Download do repositório:

Para efetuar o download do repositório na DragonBoard410c execute o comando abaixo:
```sh
cd /home/$USER
git clone https://gitlab.com/qualcomm-iot-reference-center/smart-hydrometer.git 
cd smart-hydrometer
```

Para executar a aplicação, entre no terminal e execute: 
```sh
python hydro.py -m <"modelos caffe2"> -v <"camera"> -c calib.xml 
```
 
Esses parâmetros passados para a execução do programa são: 

* -m: o diretório dos arquivos criados no treinamento de rede predict_net.pb e init_net.pb; 
* -v: o diretório do vídeo de exemplo ou link da câmera IP; 
* -c: arquivo de configuração das regiões de interesse;
* -d: opção para iniciar o programa em modo de exibição (0) ou debug (1).
 

Por exemplo: 
```sh
python hydro.py -m ./ -v "./test.mp4" -c calib.xml -d 1
```

O mesmo comando pode ser executado da seguinte maneira:
```sh
sh run_hydro.sh
```

O resultado de operação é ilustrado na imagem abaixo. Cabe destacar que os números 2 e 9 não estão na região de classificação, portanto o resultado apresentado é o definido no valor inicial da execução. 

O resultado também é gravado em um arquivo de hydro_output.txt. 

 
<div align="center">
    <figure>
        <img src="/Tutorial/fig7.png">
        <figcaption>Execução do programa. </figcaption>
    </figure>
</div>


Para exibir somente a tela de apresentação:

```sh
python hydro.py -m ./ -v "./video1.mp4" -c video1.xml -d 0
```

 <div align="center">
    <figure>
        <img src="/Tutorial/form.png">
        <figcaption>Tela de exibição.</figcaption>
    </figure>
</div>


 
### Enviando dados para nuvem 

 

Após o reconhecimento dos dígitos, esses dados podem ser enviados para um servidor em nuvem. O script para conexão e envio de dados utilizam a biblioteca ibmiotf.device e a plataforma utilizada para esse processo é a IBM Watson IoT Plataform. 

Há um tutorial explicando como realizar o envio de dados através da Dragonboard 410C, e também as configurações necessárias no IBM Watson. O tutorial está disponível neste [link](https://gitlab.com/qualcomm-iot-reference-center/watson_iot). 

Esse script recebe o valor gravado no arquivo hydro_output.txt gerado pela aplicação principal. 

```python

import time 

import sys 

import uuid 

import argparse 

import ibmiotf.device 

  

def myOnPublishCallback(): 

    print("Confirmed event %s received by IoTF\n" % x) 

  

def commandProcessor(cmd): 

    print("Command received: %s" % cmd.data) 

  

def sendData(cfg, evt, data): 

    deviceOptions = ibmiotf.device.ParseConfigFile(cfg) 
    
    deviceCli = ibmiotf.device.Client(deviceOptions) 
    
    deviceCli.commandCallback = commandProcessor 

 

# Connect and send datapoint(s) into the cloud 

deviceCli.connect() 

  

data = { 'Value' : data} 

success = deviceCli.publishEvent(evt, "json", data, qos=0, on_publish=myOnPublishCallback) 

  

if not success: 

    print("Not connected to IoTF") 

  

# Disconnect the device and application from the cloud 

deviceCli.disconnect() 

  

  

authMethod = None 

  

# Initialize the properties we need 

parser = argparse.ArgumentParser() 

  

# Primary Options 

parser.add_argument('-c', '--cfg', required=True, default=None, help='configuration file') 

parser.add_argument('-d', '--data', required=True, default=None, help='data to send') 

parser.add_argument('-e', '--event', required=True, default=None, help='dashboard event') 

  

args, unknown = parser.parse_known_args() 

  

if (args.cfg is None) or (args.data is None): 

    print("cmd error!") 

else: 
    sendData(args.cfg, args.event, int(args.data)) 
```
 
IMPORTANTE: para que a execução abaixo funcione, você deve substituir o arquivo device.cfg presente no repositório que foi clonado, pelo arquivo gerado durante o tutorial do IBM Watson ([link](https://gitlab.com/qualcomm-iot-reference-center/watson_iot/blob/master/README(pt-br).md))

Caso tenha seguido a sugestão de pastas, o comando para essa operação será:
```
cp ~/watson_iot/device.cfg ~/smart-hidrometer
```


Para executar o script: 
```sh
python ibmiot_device.py -c device.cfg -e "ValueChanged" -d $(cat hydro_output.txt) 
```
 
Os parâmetros são: 
* -c: arquivo de configuração; 
* -e: evento relacionado com o dado que será publicado; 
* -d: informação que será enviada. 

 

Após executar o script, é possível visualizar os dados enviados para a plataforma. 

 
<div align="center">
    <figure>
        <img src="/Tutorial/fig8.png">
        <figcaption>Relatório de dados enviados.</figcaption>
    </figure>
</div>



### Calibração das regiões de interesse 

A etapa de calibração já foi realizada para o vídeo de exemplo e disponibilizada nesse repositório. Mesmo assim, o procedimento pode ser executado pelo arquivo calibration.py


O resultado de execução é um arquivo de configuração em XML. Tal arquivo é utilizado como referência na aplicação principal. Para executá-lo: 


```sh
python calibration.py -d <digitos> -v <"câmera"> 
```

Esses parâmetros passados para a execução do programa são: 

* -d: o número máximo de dígitos no mostrador do hidrômetro (6); 
* -v: o diretório do vídeo de exemplo ou link para câmera IP. 

 
Por exemplo: 
```sh
python calibration.py -d 6 -v "./video1.mp4" 
```
 

Durante a exibição dessa tela, é possível fazer a seleção da região de interesse. Para selecioná-la, basta pressionar a tecla espaço do teclado e com o auxílio do mouse selecionar a região do em torno do mostrador. 

Após realizar essa etapa, é necessária uma nova marcação, agora dentro do mostrador. Para tal, selecione um quadro seguindo as bordas do mostrador. 

Posteriormente, deve-se indicar com o mouse (somente um click) no centro de cada dígito. Para auxiliar essa etapa uma linha central e outra vertical serão desenhadas da tela. 

 
 <div align="center">
    <figure>
        <img src="/Tutorial/fig6.png">
        <figcaption>Seleção das regiões de interesse.</figcaption>
    </figure>
</div>

 

Após a seleção, um arquivo calib.xml será criado no mesmo diretório. Essa é uma forma de calibrar a aplicação apenas uma vez para que seja feito um mapeamento das posições dos dígitos e o resultado do reconhecimento se mostre mais preciso. 