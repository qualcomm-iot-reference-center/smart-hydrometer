from xml.dom import minidom
import xml.etree.ElementTree as ET
from StringIO import StringIO
import numpy as np

def GetIntArrayFromString(data):
	numbers_from_file = np.loadtxt(StringIO(data), dtype=np.int, delimiter=',', usecols=(0, 1, 2, 3))

	return numbers_from_file


def ReadCalibrationFile(config_file):

	mydoc = minidom.parse(config_file)
	items = mydoc.getElementsByTagName('item')

	main = []
	roif = []

	if len(items) > (1):
		main = GetIntArrayFromString(items[0].childNodes[0].data)

		for i in range(len(items)-1):  
			data = GetIntArrayFromString(items[i+1].childNodes[0].data)
			roif.append(data)

	return main,roif
