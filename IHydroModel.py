from abc import ABCMeta,abstractmethod
import sys,os


class IHydrometerModel:
	__metaclass__ = ABCMeta

	@abstractmethod
	def Classify(self,numbers,Box):
		pass
