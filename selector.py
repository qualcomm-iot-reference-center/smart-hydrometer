import cv2


class RoiCreator:
    def __init__(self,wdname):
        self.xI = 0
        self.yI = 0
        self.xF = 0
        self.yF = 0
        self.selected = False
        self.ended = False
        self.wdname = wdname
    def updating(self, event, x, y, flags, param):
        if event == cv2.EVENT_LBUTTONDOWN:
            self.selected = True
            self.xI = x
            self.yI = y
            self.xF = x
            self.yF = y
        elif event == cv2.EVENT_MOUSEMOVE:
            #if self.selected is True and self.ended is False:
            self.xF = x
            self.yF = y
        elif event == cv2.EVENT_LBUTTONUP:
            self.ended = True

    def running(self):
        cv2.setMouseCallback(self.wdname, self.updating)

