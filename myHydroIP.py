import cv2
import imutils as imt
import numpy as np
import sys,os

sys.path.append(os.getcwd())
import IHydroIP
import IPutils
import rotate


class myHydrometerIP(IHydroIP.IHydrometerIP):

	def __init__(self,debug_flag):
		self.DEBUG = debug_flag


	def GetNumbersFromImage(self,orig,framec,iDigits,roif):
	    
	    BlackBox = []

	    numbers = [[0] * 1 for i in range(iDigits)]
	    point0 = roif[0]

	    for i in range(iDigits):
		newBox = np.zeros([28,28])
		BlackBox.append(newBox)
		numbers[i] = -1

	    # reference mark
	    hBox = point0[1] + int((point0[3] - point0[1]) / 2)


	    if self.DEBUG == 1:
		    cv2.namedWindow('Image')
		    cv2.namedWindow('Filtered')
		    cv2.moveWindow('Filtered', 50, 30)
		    cv2.namedWindow('Contours')
		    cv2.moveWindow('Contours', 400, 30)

            print(len(cv2.findContours(framec.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)))
	    _, ctrs, hier = cv2.findContours(framec.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
	    
	    #digit Skeletonization
	    img = cv2.adaptiveThreshold(framec, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, 51, 1)
	    size = np.size(img)
	    skel = np.zeros(img.shape,np.uint8)
	    element = cv2.getStructuringElement(cv2.MORPH_CROSS,(3,3))
	    done = False
	    
	    while( not done):
		    eroded = cv2.erode(img,element)
		    temp = cv2.dilate(eroded,element)
		    temp = cv2.subtract(img,temp)
		    skel = cv2.bitwise_or(skel,temp)
		    img = eroded.copy()
		 
		    zeros = size - cv2.countNonZero(img)
		    if zeros==size:
			done = True

	    #verifies if each digit is inside classification area
	    for i in range(iDigits):
		point = roif[i + 1]
		roi = None
	
		for ctr in ctrs: 
		   [x,y,w,h] = cv2.boundingRect(ctr)

		   if ((x) <= int(point[0])) and ((x+w) >= int(point[0])):
			if y <= (hBox-10):
				roi = skel[y:y+h,x:x+w]
		   		break
		
		#creates a blackbox
		if roi is not None:
			
			newBox = np.zeros([28,28])
			r = 20. / roi.shape[0]
			dim = (roi.shape[1],int(roi.shape[0] * r))
			 
			# perform the actual resizing of the image and show it		
			roi = cv2.resize(roi, dim)
	
			yi = int(newBox.shape[0] / 2) - int(roi.shape[0] / 2) 
			yf = yi + int(roi.shape[0]) 
			xi = int(newBox.shape[1] / 2) - int(roi.shape[1] / 2) 
			xf = xi + int(roi.shape[1]) 

			#Copy roi into black box
			newBox[yi:yf, xi:xf] = roi
			BlackBox[i] = newBox

			if self.DEBUG == 1:
				cv2.imshow('Image'+str(i), newBox)
	
	        	numbers[i] = 0

	    	    
	    	    	        
	    if self.DEBUG == 1:
		cv2.imshow('Imagev', framec)
		cv2.imshow('skel', skel)
	    	cv2.imshow('Image', orig)

	    return numbers,BlackBox



	def PreProcessing(self,frame,x,y,w,h):

                (img) = IPutils.CropAndResize(frame, x, y, h, w)
		img = IPutils.increase_brightness(img, 0)

		#remove background
		lower = np.array([40,40, 40], dtype = "uint8")
		upper = np.array([110, 110, 255], dtype = "uint8")
		mask = cv2.inRange(img, lower, upper)
		img_c = cv2.bitwise_and(img, img, mask = mask)


		img_c = cv2.cvtColor(img_c, cv2.COLOR_BGR2GRAY)

		return img, img_c



    
