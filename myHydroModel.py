import sys,os

sys.path.append(os.getcwd())
import IHydroModel
import model
import mnist


class myHydrometerModel(IHydroModel.IHydrometerModel):

	def __init__(self,debug_flag,CURRENT_FOLDER):
		self.DEBUG = debug_flag

		print ('\n********************************************')
		print ('loading caffe model')
		print ('********************************************')
		init_pb = os.path.join(CURRENT_FOLDER, 'init_net.pb')
		predict_pb = os.path.join(CURRENT_FOLDER, 'predict_net.pb')
		self.mnist_model = mnist.Caffe2MNIST('mnist_deploy')
		self.mnist_model.LoadPreTrainedModels(init_pb, predict_pb)



	def Classify(self,numbers,Box):

	    outputs = [[0] * 2 for i in range(len(numbers))]

	    for i in range(len(numbers)):
		outputs[i][0] = -1
		outputs[i][1] = -1

	    for i,val in enumerate(numbers):
		if(val==0):
			# run caffe2 to get the output class of especified number
			num,acc = self.mnist_model.Run(Box[i],"data")
			outputs[i][0] = num
			outputs[i][1] = acc*100
	    return outputs
		
