import tkinter
import PIL.Image, PIL.ImageTk, PIL.ImageOps
from tkinter import Frame, Label
import os

class App:
    def __init__(self, window_title, data, iDigits,ppipe):

	self.flag = ppipe
        self.qdata = data
        self.window = tkinter.Tk()

        self.window.title(window_title)
        self.window.configure(background='white') 
	window_width, window_height = 700,700 #tamanho
	self.window.geometry('{}x{}'.format(window_width, window_height))

        self.frame3 = Frame(self.window)
        self.frame3.pack(side=tkinter.TOP, padx= 0, pady= 30)

        #self.frame2 = Frame(self.window, bg="white")
        #self.frame2.pack(side=tkinter.RIGHT, padx= 30, pady= 30)

        self.frame1 = Frame(self.window, bd=2, bg="black")
        self.frame1.pack(side=tkinter.TOP, padx=50, pady = 50)


	self.mostrador = Frame(self.window, width=window_width, height=100, bd=2, bg="white")
	self.mostrador.place(relx = 0.5, rely = 0.5, anchor=tkinter.S)
	self.mostrador.pack(side=tkinter.BOTTOM)



	self.frame4 = Frame(self.window, width=window_width,bg="white")
	self.frame4.place(anchor=tkinter.S)
	self.frame4.pack(side=tkinter.BOTTOM)

	self.digits = []
	self.canvas_d = []
	self.pics = []
	self.label_list = []

	for i in range(iDigits):
	        dg = Frame(self.window, bd=2)
	        dg.pack()
	        cv = tkinter.Canvas(self.mostrador, width=100, height=100, background="black")
	        cv.pack(side=tkinter.LEFT)
	        cv.create_window(0, 0, window = dg)

		
		canvas_lbl = tkinter.Canvas(self.frame4, width=5, height=5, bg="white")
	        canvas_lbl.pack(side=tkinter.LEFT, fill="both")
            	self.label_list.append(canvas_lbl)
		self.digits.append(dg)
		self.canvas_d.append(cv)
		self.pics.append(None)


        lbl1= tkinter.Label(self.frame3, text="Smart Water Meter Solution", font=("Helvetica", 20, "bold"), fg="blue", bg="white").pack()


    def Start(self, w, h):
	self.canvas = tkinter.Canvas(self.frame1, width = w, height = h, bd=0, bg="white")
	self.canvas.pack()
        self.delay = 200
        self.photo = None
        self.update()
        self.window.mainloop()
	
	self.flag.set()


    def update(self):

        frame,boxes,value = self.qdata.get()

        if frame is not None:
            self.photo = PIL.ImageTk.PhotoImage(image = PIL.Image.fromarray(frame))
            self.canvas.create_image(0, 0, image = self.photo, anchor = tkinter.NW)

            for i in range(len(self.pics)):
		img = PIL.Image.fromarray(boxes[i])
		img = img.resize((100, 100), PIL.Image.ANTIALIAS)
            	self.pics[i] = PIL.ImageTk.PhotoImage(img)
            	self.canvas_d[i].create_image(0, 0, image = self.pics[i], anchor = tkinter.NW)
		
		ind = tkinter.Label(self.label_list[i], text=value[i], width=14, bg="white").grid(row=i, column=0)
                self.label_list[i].create_window(0, 0, window=ind)
   
            self.window.after(self.delay, self.update)
        else:
            self.window.destroy()

