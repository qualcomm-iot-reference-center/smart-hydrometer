import sys,os
import numpy as np
import itertools

sys.path.append(os.getcwd())
import IHydroCore
import IPutils
import rotate


class myHydrometerCore(IHydroCore.IHydrometerCore):

	def __init__(self,debug_flag, ip, digits, model):
		self.DEBUG = debug_flag
		self.hip = ip
		self.MAX_NUMBERS = digits
		self.Model = model 

		# create a list of n elements, each of which is a list of MAX_SAMPLES number
		self.MAX_SAMPLES = 7
		self.samples = [[0] * self.MAX_SAMPLES for i in range(self.MAX_NUMBERS)]
		self.samples_ratio = [[0] * self.MAX_SAMPLES for i in range(self.MAX_NUMBERS)]
		self.samples_counter = np.zeros(self.MAX_NUMBERS, dtype=int)


	def Process(self,frame,x,y,w,h,roif):

		img, thresh = self.hip.PreProcessing(frame,x,y,w,h);


                numbers,BlackBox = self.hip.GetNumbersFromImage(
                    img,thresh,
                    self.MAX_NUMBERS,
                    roif,
                    )

		outputs = self.Model.Classify(numbers, BlackBox)

		(num, ratio) = self.__UpdateSamples(
	                outputs,
	                self.samples,
	                self.samples_ratio,
	                self.samples_counter,
	                self.MAX_NUMBERS,
	                self.MAX_SAMPLES,
	                )

		return num,ratio,BlackBox


	def __UpdateSamples(self,numbers,samples,samples_ratio,samples_counter,MAX_NUMBERS,MAX_SAMPLES):

		ratio = np.zeros(MAX_NUMBERS, dtype=float)
	    	num = np.ones(MAX_NUMBERS, dtype=int) * -1

		for pos, (nclass, hit) in enumerate(numbers):
		    if nclass != -1:
			if hit > 50:
			    k = samples_counter[pos]
			    samples[pos][k] = nclass
			    samples_ratio[pos][k] = hit
			    k = k + 1

			    if k == MAX_SAMPLES:
				result = np.zeros(10, dtype=int)
				for (key, group) in itertools.groupby(samples[pos]):
				    count = len(list(group))
				    result[key] = result[key] + count

				idx = np.argmax(result)

				acc_ratio = 0
				for i in range(MAX_SAMPLES):
				    if samples[pos][i] == idx:
				        acc_ratio = acc_ratio + samples_ratio[pos][i]

				ratio[pos] = float(acc_ratio / result[idx])

				if ratio[pos] > 55:
				    num[pos] = idx

				k = 0

			    samples_counter[pos] = k

		return num,ratio
		
