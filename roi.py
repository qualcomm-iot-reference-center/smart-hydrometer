#!/usr/bin/python
# -*- coding: utf-8 -*-
import cv2
import imutils as imt
from selector import RoiCreator
import numpy as np


def draw_roi(pos, frame, wdname):
    frame_draw = frame.copy()
    cv2.rectangle(frame_draw, (pos[0], pos[1]), (pos[2], pos[3]), (255,
                  0, 0), 1)
    cv2.imshow(wdname, frame_draw)
    cv2.waitKey(80)


def draw_roi2(pos, frame_draw, wdname):
    cv2.rectangle(frame_draw, (pos[0], pos[1]), (pos[2], pos[3]), (255,
                  0, 0), 1)
    cv2.imshow(wdname, frame_draw)


def draw_rois(
    rois,
    frame,
    wdname,
    xf,
    ):
    frame_draw = frame.copy()

    for (i, roi) in enumerate(rois):
        draw_roi2(roi, frame_draw, wdname)

    roi_line = rois[0]
    y1 = int((roi_line[1] + roi_line[3]) / 2)
    line = [roi_line[0], y1, roi_line[2], y1]
    draw_roi2(line, frame_draw, wdname)

    line = [xf, 0, xf, y1 * 2]
    draw_roi2(line, frame_draw, wdname)

    cv2.waitKey(10)


def getROI(frame):
    wdname = 'RoiSelector'
    roi = RoiCreator(wdname)
    cv2.namedWindow(wdname)
    while roi.ended is False:
        roi.running()
        roiF = (roi.xI, roi.yI, roi.xF, roi.yF)
        draw_roi(roiF, frame, wdname)
    return roiF


def getROI2(frame, idigits):
    wdname = 'RoiSelector'
    rois = []
    roi = RoiCreator(wdname)
    cv2.namedWindow(wdname)

    while roi.ended is False:
        roi.running()
        roiF = [roi.xI, roi.yI, roi.xF, roi.yF]
        draw_roi(roiF, frame, wdname)

    # print('roi selected', roiF)

    rois.append(roiF)

    for i in range(idigits):
        roi = RoiCreator(wdname)
        draw_rois(rois, frame, wdname, 0)
        while roi.ended is False:

        # print('select roi {}'.format(i))

            roi.running()
            roiF = [roi.xI, roi.yI, roi.xF, roi.yF]
            draw_rois(rois, frame, wdname, roi.xF)

        # print('roi {} selected'.format(i))
	print(roiF)
        rois.append(roiF)

    draw_rois(rois, frame, wdname, 0)

    return rois

