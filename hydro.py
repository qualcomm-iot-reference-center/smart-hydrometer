#!/usr/bin/python
# -*- coding: utf-8 -*-

import imutils as imt
import numpy as np
import cv2
import sys
import os
import argparse



import cv2
import time

from multiprocessing import Pool, Process, Manager, Queue,Event

# import call method from subprocess module

from subprocess import call

# some scripts are imported from current working directory

sys.path.append(os.getcwd())

import rotate

import AppHydro
import CalibrationXML
import myHydroIP
import myHydroCore
import myHydroModel



# import ibmiot_device

DEBUG = 0

def GetValue(current_value, lenght):
    lenght = pow(10, lenght)
    total = 0

    for i in current_value:
        total = total + i * lenght
        lenght = lenght / 10
    return total

def PrintNumbers(current_value,ratio, MAX_NUMBERS):
        # check and make call for specific operating system
    	_ = call(('clear' if os.name == 'posix' else 'cls'))

	for j in range(MAX_NUMBERS):
	    print ('Current Output class[{}] = {} : {}'.format(j,current_value[j], ratio[j]))

def SaveCurrentValue(current_value, MAX_NUMBERS):
    file = open('hydro_output.txt', 'w')
    file.write(str(current_value))
    file.close()

def UpdateNumbers(num, ratio, current_value, current_ratio, MAX_NUMBERS):

	new_value = np.zeros(MAX_NUMBERS, dtype=int)
	new_ratio = np.zeros(MAX_NUMBERS, dtype=float)

	for j in range(MAX_NUMBERS):
		if num[j] >= 0:
			new_value[j] = num[j]
			new_ratio[j] = ratio[j]
		else:
			new_value[j] = current_value[j]
			new_ratio[j] = current_ratio[j]

	return new_value,new_ratio



def main(CURRENT_FOLDER, VIDEO, CFG):
    
    (reg, roif) = CalibrationXML.ReadCalibrationFile(CFG)

    if roif is not None:

        y = reg[1]
        h = reg[3]
        x = reg[0]
        w = reg[2]

        if y > 0 and h > 0 and x > 0 and w > 0:

            MAX_NUMBERS = len(roif) - 1
	    # initialize an array to store measured values from hydrometer and fill this array with the argument informed
	    current_value = np.ones(MAX_NUMBERS, dtype=int)*-1
            current_ratio = np.zeros(MAX_NUMBERS, dtype=float)

	    #Form
            myq = Queue()
            evt = Event()
            window = AppHydro.App("", myq, MAX_NUMBERS,evt)
            process_one = Process(target=window.Start, args=(420,340))
            process_one.start()


	    hip = myHydroIP.myHydrometerIP(DEBUG)
	    hmodel = myHydroModel.myHydrometerModel(DEBUG,CURRENT_FOLDER)	
	    hcore = myHydroCore.myHydrometerCore(DEBUG,hip,MAX_NUMBERS,hmodel)


            vobj = cv2.VideoCapture(VIDEO)

	    if DEBUG == 1:
            	cv2.namedWindow('Frame')
            	cv2.moveWindow('Frame', 600, 30)
                
            while True:
                (status, frame) = vobj.read()
		
                if (status is False) or (frame is None):
                    vobj.release()
                    myq.put(None)
		    break


		frame = rotate.rotate_bound(frame, 90)
                frame = cv2.resize(frame, (420, 340))

		num,ratio,BlackBox = hcore.Process(frame,x,y,w,h,roif)

                if num is not None:
  
	            (current_value, current_ratio) = UpdateNumbers(num,
	                    ratio, current_value, current_ratio,
	                    MAX_NUMBERS)

	            #update App Form		   
	            img2window = cv2.cvtColor(frame.copy(),cv2.COLOR_BGR2RGB)
		    lbl_txt = []
		    for i in range(MAX_NUMBERS):
			txt = str(current_value[i]) + ": " + str(round(current_ratio[i],2))
		    	lbl_txt.append(txt)

	            myq.put([img2window,BlackBox,lbl_txt])

	            if DEBUG == 1:
			PrintNumbers(current_value, current_ratio,MAX_NUMBERS)
                	cv2.imshow('Frame', frame)
			
			check = 0	
			for i in range(len(current_value)):
				if(current_value[i]==-1):
					check = -1
			if(check!=-1):
				SaveCurrentValue(current_value, MAX_NUMBERS)

			key = cv2.waitKey(1) & 0xFF
		    
			if(key == ord('q')):
				myq.put(None)
			    	break
			

		if evt.is_set():
			break

            vobj.release()
            cv2.destroyAllWindows()

	    myq.close()
	    myq.join_thread() 

            process_one.join()





if __name__ == '__main__':

    # Initialize the properties we need

    parser = argparse.ArgumentParser()

    # Primary Options

    parser.add_argument('-m', '--caffe_model', required=True, default=''
                        , help='caffe model dir')
    parser.add_argument('-v', '--video', required=True, default='',
                        help='source video')
    parser.add_argument('-c', '--cfg', required=True, default=None,
                        help='configuration file')
    parser.add_argument('-d', '--debug', required=True, default=None,
                        help='configuration file')

    (args, unknown) = parser.parse_known_args()

    if args.cfg is None:
        print ('error')
    else:
	if int(args.debug) > 0:
		DEBUG = 1
        main(args.caffe_model,args.video, args.cfg)
