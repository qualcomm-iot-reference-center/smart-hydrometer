#!/usr/bin/python
# -*- coding: utf-8 -*-
import cv2
import imutils as imt
import numpy as np
import sys
import os
import argparse
import itertools
from xml.dom import minidom
import xml.etree.ElementTree as ET

# import call method from subprocess module

from subprocess import call

# some scripts are imported from current working directory

sys.path.append(os.getcwd())
import rotate
import roi


def CreateCalibrationFile(roi,roi_n,i_max):

	print(roi,roi_n)

	# create the file structure
	data = ET.Element('calibration')  
	items = ET.SubElement(data, 'rois') 
 
	item1 = ET.SubElement(items, 'item')
	item1.set('name','main')
	item1.text = '{},{},{},{}'.format(str(roi[0]),str(roi[1]),str(roi[2]),str(roi[3]))
	
	for i in range(i_max):
		num = roi_n[i]
		item2 = ET.SubElement(items, 'item')
		item2.set('name','roi{}'.format(str(i)))
		item2.text = '{},{},{},{}'.format(str(num[0]),str(num[1]),str(num[2]),str(num[3]))
  
	# create a new XML file with the results
	mydata = ET.tostring(data)  
	myfile = open("./calib.xml", "w")  
	myfile.write(mydata)  


def main(
    MAX_NUMBERS,
    VIDEO
    ):


    y = 0
    h = 0
    x = 0
    w = 0

    vobj = cv2.VideoCapture(VIDEO)

    cv2.namedWindow('Frame')
    cv2.moveWindow('Frame', 600, 30)

    while True:
        (status, frame) = vobj.read()

        if status is False:
            vobj.release()
	    #break

        # rotate and resize original frame
        frame = rotate.rotate_bound(frame, 90)
        frame = cv2.resize(frame, (420, 340))
        cv2.imshow('Frame', frame)

        key = cv2.waitKey(20) & 0xFF

        if key == ord(' '):

		# define ROI
		reg = roi.getROI(frame)
		y = reg[1]
		h = reg[3]
		x = reg[0]
		w = reg[2]
		roif = []

		if y > 0 and h > 0 and x > 0 and w > 0:
			framec = frame[y:h, x:w]
		    	framec = cv2.resize(framec, (293, 68))
			roif = roi.getROI2(framec, MAX_NUMBERS)

			CreateCalibrationFile(reg,roif,MAX_NUMBERS+1)

			break; #exit



    vobj.release()
    cv2.destroyAllWindows()


if __name__ == '__main__':

    # Initialize the properties we need

    parser = argparse.ArgumentParser()

    # Primary Options

    parser.add_argument('-d', '--max_numbers', required=True,
                        default=6, help='number of digits')
    parser.add_argument('-v', '--video', required=True, default='',
                        help='source video')

    (args, unknown) = parser.parse_known_args()


    main(int(args.max_numbers),args.video)

