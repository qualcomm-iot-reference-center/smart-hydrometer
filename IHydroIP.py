from abc import ABCMeta,abstractmethod

class IHydrometerIP:
	__metaclass__ = ABCMeta

	@abstractmethod
	def GetNumbersFromImage(self,orig,framec,iDigits,roif):
		pass

	@abstractmethod
	def PreProcessing(self,frame,x,y,w,h):
		pass
