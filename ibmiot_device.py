import time
import sys
import uuid
import argparse
import ibmiotf.device

def myOnPublishCallback():
	print("Confirmed event %s received by IoTF\n" % x)

def commandProcessor(cmd):
	print("Command received: %s" % cmd.data)

def sendData(cfg, evt, data):
	deviceOptions = ibmiotf.device.ParseConfigFile(cfg)
	deviceCli = ibmiotf.device.Client(deviceOptions)
	deviceCli.commandCallback = commandProcessor
	
	# Connect and send datapoint(s) into the cloud
	deviceCli.connect()

	data = { 'Value' : data}
	success = deviceCli.publishEvent(evt, "json", data, qos=0, on_publish=myOnPublishCallback)

	if not success:
		print("Not connected to IoTF")

	# Disconnect the device and application from the cloud
	deviceCli.disconnect()


authMethod = None

# Initialize the properties we need
parser = argparse.ArgumentParser()

# Primary Options
parser.add_argument('-c', '--cfg', required=True, default=None, help='configuration file')
parser.add_argument('-d', '--data', required=True, default=None, help='data to send')
parser.add_argument('-e', '--event', required=True, default=None, help='dashboard event')

args, unknown = parser.parse_known_args()

if (args.cfg is None) or (args.data is None):
	print("cmd error!")
else:
   sendData(args.cfg, args.event, int(args.data))

