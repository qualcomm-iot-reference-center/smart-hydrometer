# Intelligent hydrometer using OpenCV and Caffe2 in DragonBoard 410C

Project developed at the IoT Reference Center, between Qualcomm and Facens. 
This article presents an application executed in DragonBoard410c, based on the concepts of AI, which does the digits recognition of a Hydrometer using OpenCV and Caffe2.

 

# Introduction

The developed application uses a camera to perform the monitoring of a hydrometer. For demonstration and proof of concept, an IP camera was used to enable monitoring remotely. With this, the images of the hydrometer are obtained via the network and processed in DragonBoard410c.


 <div align = "center">
    <figure>
        <img src = "/Tutorial/fig1.png">
        <figcaption> Hydrometer used in the project. </figcaption>
    </figure>
</div>


It should be noted that the image obtained is dependent on the operating environment. Therefore, it is necessary to treat the images seeking a better adaptation to the environment in which the project is implemented.

The analyzed image defines a region of interest that contains all the digits of the hydrometer display. Subsequently, each digit present in the image is segmented and processed by a convolutional network (CNN) to classify the number.

 

 <div align = "center">
    <figure>
        <img src = "/Tutorial/fig2.png">
        <figcaption> Region of interest for recognition. </figcaption>
    </figure>
</div>



 

For the development of the project, the OpenCV library was used to segment the digits, and the Caffe2 framework was used to perform digit recognition.

To recognize the digits in the hydrometer display, a CNN was trained with the MNIST dataset.

 

You need to perform network training before running the project script.

To understand how training occurs and how to run the model in DragonBoard 410C, visit the article [MNIST: Creating a CNN for DragonBoard 410C](https://gitlab.com/qualcomm-iot-reference-center/mnist-digit-recognition-tutorial) that contains all the instructions for installation, training, and execution of the network.

 

## Hardware Used

 

> This application was run on the DragonBoard410c development platform. The image of the operating system is linaro-buster-alip-dragonboard-410c-359.img.gz and can be obtained at this link: https://releases.linaro.org/96boards/dragonboard410c/linaro/debian/latest/

 

> Kernel version:

 

> Linux qubuntu 4.15.0-36-generic # 39-Ubuntu SMP Mon Sep 24 16:19:09 UTC 2018 x86_64 x86_64 x86_64 GNU / Linux

 

> It should be noted that the system uses an external memory to expand the capacity of the rootfs and provide a swap region. The configuration of this mechanism is as follows: 4GB swap and the rest dedicated to the rootfs.

 

### Equipment

 

The equipment used to implement the project are:

* DragonBoard 410C;
* 12V, 2A Wall adapter;
* IP camera and ethernet point;
* Water Meter Hydrometer.

 

### Installing Python libraries

 

For the installation of the libraries used in the project, execute the commands below in the terminal of DragonBoard410c:

```sh

pip install numpy

pip install imutils

pip install uuid

pip install more-itertools

pip install ibmiotf

sudo apt-get install python-imaging-tk

sudo apt-get install python-opencv
```
 

#### Caffe2 Framework Installation

 

The Caffe2 framework requires an installation from the source code and has other dependencies that will be requested during the installation. The article on Caffe2 and its installation on the DragonBoard 410C can be found [at this link](https://gitlab.com/qualcomm-iot-reference-center/caffe2-installation-and-test). If any of the terms discussed during this article are not known, or to learn more about Caffe2, visit the Caffe2.ai guides.

 

#### IBM Cloud

 

The ibmiotf.device library is used to send data to the chosen platform. If another platform is chosen to send data, you should check the dependencies.

 
### Application Structure

 

Below is a brief explanation of the main parts of the script that were implemented in the project to understand the use of computer vision.

 

#### Image Processing

 

One of the main parts of the project is to handle the images that are captured by the camera. For this, applications of filters are realized for noise reduction, color space conversion, highlighting, among others.

The filtering performed on the image is very important so that you can subsequently detect the contours of the digits. For this, we used filters that help in the detection of the edges and keep them sharp.

The purpose of these procedures is to highlight the segmented digits that will be used for recognition in the best possible way. The following is the result of processing, resulting in a black and white image with the highlighted digits.

 
 <div align = "center">
    <figure>
        <img src = "/Tutorial/fig3.png">
        <figcaption> Filters applied to enable digit segmentation. </figcaption>
    </figure>
</div>


For image processing it is necessary to convert to grayscale (1 channel), apply smoothing filters, edge enhancement, and binarization. Subsequently, morphological operations are used to clean the image noises and dilate the contours found. These techniques and other types of filters that can be applied are found in the links below, which contains all the documentation of the functions available in the library.

> OpenCV documentation for the functions used:
> * Color space;
> * Laplacian;
> * Bilateral;
> * Thresholding;
> * Morphological transformation.

 

#### Digit Targeting

 

After the images are processed, it is possible to find the contours and map the digit positions to allow the recognition process. This operation is performed considering the position of the contours found and the references defined in the calibration process of the regions of interest.

Thus, for each contour identified, the geometric center (highlighted points) is calculated to validate if the digit is appearing entirely in the display. For this, the geometric center is compared with the calibration of each digit.

Subsequently, each digit is converted to the input standard of the network that will perform the classification.


 <div align = "center">
    <figure>
        <img src = "/Tutorial/fig4.png">
        <figcaption> Segmentation of the regions of interest. </figcaption>
    </figure>
</div>

 

 

> OpenCV documentation for the functions used:
> * Find Contours;
> * Gaussian Filtering.

 

#### Digit Recognition

 

As previously stated, the application segments the numbers of the original image by generating a new image for each one. Then, the generated images are processed to serve as inputs to the trained model.

One of the essential scripts for recognition with Caffe2 is model.py, where you can find the entire framework for loading pre-trained templates. To load models, the Caffe2Model class provides the LoadPreTrainedModels method.

To run the model you need to fill in the network entry. This operation is performed by a specialization of the Caffe2Model class, called Caffe2MNIST and defined in the mnist.py file.

The Run method converts the image (roi) to the input pattern of the network and fills the input blob. Subsequently, the network is executed having as an output the confidence for each digit. Therefore, the highest value index is considered as the recognized digit.

```python
res = model.run_caffe2 (roi) * 100


max_class = np.argmax (res)
```
 

Thus, for each digit in the classification region, a percentage value is obtained. The result will be as in the figure below.


 <div align = "center">
    <figure>
        <img src = "/Tutorial/fig5.png">
        <figcaption> Rating result. </figcaption>
    </figure>
</div>

 

It should be noted that the program only performs the confirmation of one digit with an accuracy above 85%. Besides, some samples are stored so that decision-making is performed with greater confidence.

 

## Running the DragonBoard Code

Following are the steps required to run the code on the DragonBoard 410C. Make sure all libraries have been installed and downloaded from the repository, steps that are at the beginning of this tutorial and must be run first.


Also, the program contains a graphical interface. Then, you can run it in two ways:

1. With the dragonboard connected to a monitor via the HDMI port.
2. With a connection via ssh (video refresh rate is low)

For SSH connection, just run the following command on the HOST:

```sh
ssh -X <USER>:<IP TARGET>
```

In that, USER is the user on the dragonboard, and IP is the IP address of the adapter.


### Download Repository

 
To download the repository in DragonBoard410c run the command below:

```sh
cd /home/$USER
git clone https://gitlab.com/qualcomm-iot-reference-center/smart-hydrometer.git 
cd smart-hydrometer
```

To run the application, enter the terminal and execute:

```sh
python hydro.py -m <"modelos caffe2"> -v <"camera"> -c calib.xml 
```
 
These parameters passed to the execution of the program are:

* -m: the directory of the files created in the network training predict_net.pb and init_net.pb;
* -v: the sample video directory or IP camera link;
* -c: configuration file of the regions of interest;
* -d: option to start the program in view (0) or debug (1).
 

For example:

```sh
python hydro.py -m ./ -v "./test.mp4" -c calib.xml -d 1
```

The same command can be executed as follows:

```sh
sh run_hydro.sh
```


The operating result is shown in the image below. Note that the numbers 2 and 9 are not in the classification region, so the result presented is the one defined in the initial value of the execution.

The result is also written to a file in hydro_output.txt.

<div align = "center">
    <figure>
        <img src = "/Tutorial/fig7.jpg">
        <figcaption> Running the program. </figcaption>
    </figure>
</div>


To display only the splash screen:

```sh
python hydro.py -m ./ -v "./test.mp4" -c calib.xml -d 0
```

 <div align = "center">
    <figure>
        <img src = "/Tutorial/forms.png">
        <figcaption> Display screen. </figcaption>
    </figure>
</div>


 

### Sending data to the cloud

 

After digit recognition, this data can be sent to a cloud server. The script for connecting and sending data uses the ibmiotf.device library and the platform used for this process is IBM Watson IoT Plataform.

There is a tutorial explaining how to send data through the Dragonboard 410C, as well as the necessary settings in IBM Watson. The tutorial is available at this link.

This script receives the value written to the hydro_output.txt file generated by the main application.



```python
import time

import sys

import uuid

import argparse

import ibmiotf.device

  

def myOnPublishCallback ():

    print ("Confirmed event% s received by IoTF \ n"% x)

  

def commandProcessor (cmd):

    print ("Command received:% s"% cmd.data)

  

def sendData (cfg, evt, data):

    deviceOptions = ibmiotf.device.ParseConfigFile (cfg)
    
    deviceCli = ibmiotf.device.Client (deviceOptions)
    
    deviceCli.commandCallback = commandProcessor

 

# Connect and send datapoint (s) into the cloud

deviceCli.connect ()

  

data = {'Value': date}

success = deviceCli.publishEvent (evt, "json", date, qos = 0, on_publish = myOnPublishCallback)

  

if not success:

    print ("Not connected to IoTF")

  

# Disconnect the device and application from the cloud

deviceCli.disconnect ()

  

  

authMethod = None

  

# Initialize the properties we need

parser = argparse.ArgumentParser ()

  

# Primary Options

parser.add_argument ('- c', '--cfg', required = True, default = None, help = 'configuration file')

parser.add_argument ('- d', '--data', required = True, default = None, help = 'data to send')

parser.add_argument ('-e','event ', required = True, default = None, help =' dashboard event ')

  

args, unknown = parser.parse_known_args ()

  

if (args.cfg is None) or (args.data is None):

    print ("cmd error!")

else:
    sendData (args.cfg, args.event, int (args.data))
```

IMPORTANT: In order for the code to work, you must replace the device.cfg file in the cloned repository by the new file generated during the IBM Watson tutorial([link](https://gitlab.com/qualcomm-iot-reference-center/watson_iot))

If you followed the storage suggestion, the command for this will be:

```sh
cp ~/watson_iot/device.cfg ~/smart-hidrometer
```


To run the script:
```sh
python ibmiot_device.py -c device.cfg -e "ValueChanged" -d $(cat hydro_output.txt)
```
 

 

The parameters are:
* -c: configuration file;
* -e: event related to the data that will be published;
* -d: information to be sent.

 

After you run the script, you can view the data sent to the platform.

 
<div align = "center">
    <figure>
        <img src = "/Tutorial/fig8.png">
        <figcaption> Sent data report. </figcaption>
    </figure>
</div>


### Calibration of regions of interest
The calibration step has already been performed for the sample video and made available in that repository. Even so, the procedure can be performed by the calibration.py file.


The execution result is an XML configuration file. Such a file is used as a reference in the main application. To run it:

```sh
python calibration.py -d <digits> -v <"camera">
```

These parameters passed to the execution of the program are:

* -d: the maximum number of digits in the hydrometer display (6);
* -v: the sample video directory or IP camera link.

 
For example:
```sh
python calibration.py -d 6 -v "./test.mp4"
```
 

While viewing this screen, you can select the region of interest. To select it, press the space key on the keyboard and with the help of the mouse select the region around the display.

After performing this step, a new marking is required, now inside the display. To do this, select a frame by following the edges of the display.

Afterward, you should indicate with the mouse (only one click) in the center of each digit. To assist this step use the references drawn in the screen.

 
 <div align = "center">
    <figure>
        <img src = "/Tutorial/fig6.png">
        <figcaption> Selection of regions of interest. </figcaption>
    </figure>
</div>

 

After the selection, a calib.xml file will be created in the same directory. This is a way to calibrate the application only once so that the positions of the digits are mapped and the recognition result is more accurate.