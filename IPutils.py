import cv2
import imutils as imt
import numpy as np

def increase_brightness(img, value=30):
    hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
    (h, s, v) = cv2.split(hsv)
    lim = 0xFF - value
    v[v > lim] = 0xFF
    v[v <= lim] += value

    final_hsv = cv2.merge((h, s, v))
    img = cv2.cvtColor(final_hsv, cv2.COLOR_HSV2BGR)
    return img

def CropAndResize(frame, x, y, h, w, h_dst=68, w_dst=293):
	img_c = frame[y:h, x:w]
	img_c = cv2.resize(img_c, (w_dst, h_dst))

	return img_c

def findSignificantContours(
    edgeImg,
    wmin,
    wmax,
    hmin,
    hmax,
    szRoi,
    ):





    # check if OpenCV version is 2.X 4
    if imt.is_cv2() or imt.is_cv4():
        (contours, heirarchy) = cv2.findContours(edgeImg.copy(), cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)
 
    # check if OpenCV version is 3
    elif imt.is_cv3():
        (_, contours, heirarchy) = cv2.findContours(edgeImg.copy(), cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)


    significant = []

    # scans selected contours e check its bounds

    for (i, tupl) in enumerate(heirarchy[0]):
        contour = contours[tupl[0]]
        area = cv2.contourArea(contour)
        [x, y, w, h] = cv2.boundingRect(contour)

        if w >= wmin and w <= wmax and h >= hmin and h <= hmax:
            cX = int(x + w / 2)
            cY = int(y + h / 2)
            significant.append([contour, cX, cY, x, y, w, h])

    return significant
